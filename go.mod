module "https://gitlab.com/ikhlas-firlana/authorize-bearer-middleware"

go 1.12

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/joho/godotenv v1.3.0 // indirect
)

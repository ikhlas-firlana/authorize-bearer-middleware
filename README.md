# authorize-bearer-middleware

validate authorization Bearer TOKEN and continue to applications.

### Getting Started
```
cp .envexample to .env
```
Run application local
```
make run
```
